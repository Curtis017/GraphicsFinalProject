// from Khronos.org
function loadTexture(src) {
    // Create and initialize the WebGLTexture object.
    var texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

    // Create a DOM image object.
    var image = new Image();
    image.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    };
    // Start downloading the image by setting its source.
    image.src = src;
    // Return the WebGLTexture object immediately.
    return texture;
}

function loadCubeMap(){
  // From Stack OverFlow http://hristo.oskov.com/projects/cs418/mp3
  var cubeMap = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeMap);
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

  cubeFaces = [
    ["sky/interstellar_lf.png", gl.TEXTURE_CUBE_MAP_POSITIVE_X],
    ["sky/interstellar_rt.png", gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
    ["sky/interstellar_up.png", gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
    ["sky/interstellar_dn.png", gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
    ["sky/interstellar_ft.png", gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
    ["sky/interstellar_bk.png", gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
  ];

  for (var i = 0; i < cubeFaces.length; i++) {
     var face = cubeFaces[i][1];
     var image = new Image();
     image.onload = function(cubeMap, face, image) {
         return function() {
             gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeMap);
             gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
             gl.texImage2D(face, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
         }
     } (cubeMap, face, image);
     image.src = cubeFaces[i][0];
 }

  // gl.activeTexture(gl.TEXTURE0);
  // gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeMap);
  // gl.uniform1i(gl.getUniformLocation(GC.shaderProgram, 'uCubeSampler'), 0);

  return cubeMap;
}
