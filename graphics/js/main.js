mySpecialCounter = 0;
myObjectArray = [];
function executeMainLoop(filenames){
  mySpecialCounter = filenames.length;
  for (var i = 0; i < filenames.length; i++) {
    $.ajax({
      url:"./"+filenames[i],
      success:processMesh // --- call the function "processMesh" when the .obj is loaded
    });
  }
}

//function to load the mesh and setup the opengl rendering demo
function processMesh(data){
  mySpecialCounter = mySpecialCounter - 1;
  if(data.target != undefined){
    var mesh = {model: new modelLoader.Mesh(data.target.result)}
  } else {
    var mesh = {model: new modelLoader.Mesh(data)}
  }

    //create a new model viewing demo
    myDemo = new demo("glcanvas", mesh);
    myObjectArray.push(data[1]);

    if(mySpecialCounter <= 0){
      // console.log(myObjectArray);
      //setup the webgl context and initialize everything
      myDemo.init();

      //enter the event driven loop; ---- demo.js
      myDemo.MainLoop();
  }
}
