<?php
?>
<html>
<head>
<meta charset="utf-8">
<title>Model Viewer</title>
<!-- include all javascript source files -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="js/sylvester.js"></script>
<script type="text/javascript" src="js/math.js"></script>
<script type="text/javascript" src="js/glUtils.js"></script>
<script type="text/javascript" src="js/meshLoader.js"></script>
<script type="text/javascript" src="js/arcball.js"></script>
<script type="text/javascript" src="js/demo.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/curtis.js"></script>
</head>
<body>
<canvas id="glcanvas">canvas not supported</canvas>
<div>
  Note: Zoom or rotate the area to load the skybox and textures correctly.
  <p>
    Object 1: First space ship (left).<br>
    Object 2: Silver Surfer (center).<br>
    Object 3: First space ship (right).<br>
  </p>
  <p>
    Effect 1: Sky Box that surrounds the entire scene.<br>
    Effect 2: Textures on the two space ships.<br>
    Effect 3: Phong shading on the silver surfer.<br>
  </p>
</div>

<!-- Fragment Shader -->
<script id="FragmentShader1" type="x-shader/x-fragment">
    #ifdef GL_OES_standard_derivatives
        #extension GL_OES_standard_derivatives : enable
    #endif

    precision mediump float;

    varying vec3 normPos;
    varying vec2 textPos;

    void main(void){
        gl_FragColor = vec4(normPos, 1.0);
    }

</script>

<!-- Vertex Shader -->
<script id="VertexShader1" type="x-shader/x-vertex">
    attribute vec3 vPos; //vertex position
    attribute vec3 nPos; //normal position
    attribute vec2 tPos; //texture position

    uniform mat4 uMVMatrix;//modelviewmatrix
    uniform mat4 uPMatrix;//projectionmatrix
    uniform mat4 uNMatrix;//normalmatrix

    varying vec3 normPos;
    varying vec2 textPos;

    void main(void) {
      textPos = tPos;
      normPos = normalize(abs(nPos));
      gl_Position = uPMatrix * uMVMatrix * vec4(vPos, 1.0);
    }
</script>

<!-- Fragment Shader -->
<script id="fPhongShader" type="x-shader/x-fragment">
    #ifdef GL_OES_standard_derivatives
        #extension GL_OES_standard_derivatives : enable
    #endif

    precision mediump float;

    varying vec3 normPos;
    varying vec2 textPos;
    varying vec3 vectPos;

    const vec3 lightPos = vec3(0.0, 0.0, 10.0);
    const vec3 diffuseColor = vec3(0.50754, 0.50754, 0.50754);
    const vec3 specColor = vec3(0.508273, 0.508273, 0.508273);
    const vec3 ambient = vec3(0.19225, 0.19225, 0.19225);
    const float shininess = 51.2;

    void main(void){
      vec3 L = normalize(lightPos - vectPos);
      vec3 E = normalize(-vectPos);
      vec3 R = normalize(-reflect(L,normPos));

      vec3 Iamb = ambient;
      vec3 Idiff = diffuseColor * max(dot(normPos,L), 0.0);
      vec3 Ispec = specColor * pow(max(dot(R,E),0.0),0.3*shininess);

      gl_FragColor = vec4(Iamb + Idiff + Ispec, 1.0);
    }

</script>

<!-- Vertex Shader -->
<script id="vPhongShader" type="x-shader/x-vertex">
    attribute vec3 vPos; //vertex position
    attribute vec3 nPos; //normal position
    attribute vec2 tPos; //texture position

    uniform mat4 uMVMatrix;//modelviewmatrix
    uniform mat4 uPMatrix;//projectionmatrix
    uniform mat4 uNMatrix;//normalmatrix

    varying vec3 normPos;
    varying vec2 textPos;
    varying vec3 vectPos;

    void main(void) {
      vectPos = vec3(uMVMatrix * vec4(vPos,1.0));
      normPos = vec3(mat3(uNMatrix) * nPos);
      textPos = tPos;
      gl_Position = uPMatrix * uMVMatrix * vec4(vPos, 1.0);
    }
</script>

<!-- Fragment Shader -->
<script id="fTextureShader" type="x-shader/x-fragment">
    #ifdef GL_OES_standard_derivatives
        #extension GL_OES_standard_derivatives : enable
    #endif

    precision mediump float;

    varying vec3 normPos;
    varying vec2 textPos;

    uniform sampler2D textU;

    void main(void){
        gl_FragColor = texture2D(textU, textPos);
    }

</script>

<!-- Vertex Shader -->
<script id="vTextureShader" type="x-shader/x-vertex">
    attribute vec3 vPos; //vertex position
    attribute vec3 nPos; //normal position
    attribute vec2 tPos; //texture position

    uniform mat4 uMVMatrix;//modelviewmatrix
    uniform mat4 uPMatrix;//projectionmatrix
    uniform mat4 uNMatrix;//normalmatrix

    varying vec3 normPos;
    varying vec2 textPos;

    void main(void) {
      normPos = nPos;
      textPos = tPos;
      gl_Position = uPMatrix * uMVMatrix * vec4(vPos, 1.0);
    }
</script>

<!-- Fragment Shader -->
<script id="fSkyBoxShader" type="x-shader/x-fragment">
    #ifdef GL_OES_standard_derivatives
        #extension GL_OES_standard_derivatives : enable
    #endif

    precision mediump float;

    varying vec3 normPos;
    varying vec3 vertPos;
    varying vec2 textPos;

    uniform sampler2D textU;
    uniform samplerCube uCubeSampler;
    uniform highp mat4 uMVMatrix;

    void main(void){

      vec3 incident_eye = normalize(vertPos);
      vec3 normal = normalize(normPos);
      vec3 reflected = reflect(incident_eye, normal);
      vec3 reflection_angle = normalize(vec3(uMVMatrix * vec4(reflected, 0.0)));

      vec4 color = textureCube(uCubeSampler, incident_eye);
      gl_FragColor = color;
    }

</script>

<!-- Vertex Shader -->
<script id="vSkyBoxShader" type="x-shader/x-vertex">
    attribute vec3 vPos; //vertex position
    attribute vec3 nPos; //normal position
    attribute vec2 tPos; //texture position

    uniform mat4 uMVMatrix;//modelviewmatrix
    uniform mat4 uPMatrix;//projectionmatrix
    uniform mat4 uNMatrix;//normalmatrix

    varying vec3 normPos;
    varying vec3 vertPos;
    varying vec2 textPos;

    void main(void) {

      vertPos = vec3(vec4(vPos, 1.0));
      normPos = vec3(uMVMatrix * vec4(nPos, 1.0));
      textPos = tPos;
      gl_Position = uPMatrix * uMVMatrix * vec4(vPos, 1.0);
    }
</script>


<!-- Fragment Shader -->
<script id="fSurferShader" type="x-shader/x-fragment">
    #ifdef GL_OES_standard_derivatives
        #extension GL_OES_standard_derivatives : enable
    #endif

    precision mediump float;

    varying vec3 normPos;
    varying vec3 vertPos;
    varying vec2 textPos;
    varying vec3 normalizedPos;

    uniform sampler2D textU;
    uniform samplerCube uCubeSampler;
    uniform highp mat4 uMVMatrix;

    const vec3 lightPos = vec3(0.0, 0.0, 0.0);
    const vec3 diffuseColor = vec3(0.4, 0.4, 0.4);
    const vec3 specColor = vec3(0.774597, 0.774597, 0.774597);
    const vec3 ambient = vec3(0.25, 0.25, 0.25);
    const float shininess = 76.8;

    void main(void){

      vec3 incident_eye = normalize(vertPos);
      vec4 color = textureCube(uCubeSampler, incident_eye);

      vec3 L = normalize(lightPos - vertPos);
      vec3 E = normalize(-vertPos);
      vec3 R = normalize(-reflect(L,normalizedPos));

      vec3 Iamb = ambient;
      vec3 Idiff = diffuseColor * max(dot(normalizedPos,L), 0.0);
      vec3 Ispec = specColor * pow(max(dot(R,E),0.0),0.3*shininess);

      vec4 lighting = vec4(Iamb + Idiff + Ispec, 1.0);
      gl_FragColor = lighting;

    }

</script>

<!-- Vertex Shader -->
<script id="vSurferShader" type="x-shader/x-vertex">
    attribute vec3 vPos; //vertex position
    attribute vec3 nPos; //normal position
    attribute vec2 tPos; //texture position

    uniform mat4 uMVMatrix;//modelviewmatrix
    uniform mat4 uPMatrix;//projectionmatrix
    uniform mat4 uNMatrix;//normalmatrix

    varying vec3 normPos;
    varying vec3 vertPos;
    varying vec2 textPos;
    varying vec3 normalizedPos;

    void main(void) {
      vertPos = vec3(uMVMatrix * vec4(vPos, 1.0));
      normPos = vec3(uMVMatrix * vec4(nPos, 1.0));
      normalizedPos = vec3(mat3(uNMatrix) * nPos);
      textPos = tPos;
      gl_Position = uPMatrix * uMVMatrix * vec4(vPos, 1.0);
    }
</script>

<script>
    //grab the filename for the .obj we will first open
    // var filenames = ["sphere3.obj", "sphere3.obj", "sphere3.obj"];
    var filenames = ["silversurfer1.obj", "ship2.obj", "ship3.obj", "cube.obj"];

    //call the main mesh Loading function; main.js
    executeMainLoop(filenames);
</script>

</body>
</html>
